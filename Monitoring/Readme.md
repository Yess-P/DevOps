<a href="https://yess-p.notion.site/Prometheus-7ea57f31d5964eb68d011d1c3c47a499"><img src="https://img.shields.io/badge/Prometheus-E6522C?style=flat-square&logo=Prometheus&logoColor=white"/></a>
<a href="https://yess-p.notion.site/Monitoring-b8c9063ae6f340d1b9d2f4640e79cdef"><img src="https://img.shields.io/badge/Grafana-F46800?style=flat-square&logo=Grafana&logoColor=white"/></a>
<a href="https://yess-p.notion.site/Grafana-Loki-2723ec9207e74b159b3605100ac4b19b"><img src="https://img.shields.io/badge/Loki-f9bf52?style=flat-square&logoColor=white"/></a>
<a href="https://yess-p.notion.site/fluentd-fbb40d07ff1a424a8692f069907d1306"><img src="https://img.shields.io/badge/Fluentd-0E83C8?style=flat-square&logo=Fluentd&logoColor=white"/></a>

# Monitoring
Monitoring 위해 `Prometheus` `Grafana` `Loki` `Fluentd` 를 구축하였습니다

<br>

> **Diagram**
---

![Monitoring](/uploads/709bf09aaf63a672d19fb63fc8b9c851/Monitoring.png)

---

<br>

> **Prometheus Install**
---

##### Helm 설치

```bash
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh
```

##### Repository 추가
```bash
# community
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

# operator
helm repo add stable https://charts.helm.sh/stable

helm repo update
```

##### namespace 생성
```bash
kubectl create ns prom
```

##### prometheus 실행
```bash
# community
helm install -n prom  prometheus prometheus-community/kube-prometheus-stack

# operator
helm install -n prom -f pvc.yaml prometheus stable/prometheus-operator
```
<br>

> **Loki Install**
---

##### Repository 추가
```bash
helm repo add grafana https://grafana.github.io/helm-charts

helm repo update
```

##### namespace 생성
```bash
kubectl create ns loki
```

##### Loki 실행
```bash
helm install -n loki loki grafana/loki-stack
```

##### Portforward
```bash
k port-forward deployment/prometheus-grafana 3000
```

<br>

> **Loki Data Source 추가**
---

##### Menu Bar → configuration → Data Source 클릭

![first](/uploads/d40f2aad127670116fd2c3b07990524f/first.jpg)

<br>

##### Add data source → Loki select

![second](/uploads/8b2d0c7995350295cae3a04d1240f6ff/second.png)

<br>

##### URL에 kubernetes에 띄어진 Loki service의 cluster IP와 port 등록 후 밑에 있는 Save & Test 진행

![third](/uploads/0c13f19e0ebdcc627bffca8a06b058bc/third.png)

<br>

##### DashBoard 에 있는 Manage 확인

![fourth](/uploads/5a8bd4eb32e43230196491eae9a8fe13/fourth.png)

<br>

> **Fluentd Install**
---

##### Daemonset을 통해 Fluentd 배포
```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: fluentd
  namespace: kube-system
  labels:
    app: fluentd-logging
    version: v1
    kubernetes.io/cluster-service: "true"
spec:
  selector:
    matchLabels:
      name: fluentd-logging
      version: v1
      kubernetes.io/cluster-service: "true"
  template:
    metadata:
      labels:
        name: fluentd-logging
        version: v1
        kubernetes.io/cluster-service: "true"
    spec:
      tolerations:
      - key: node-role.kubernetes.io/master
        effect: NoSchedule
      containers:
      - name: fluentd
        image: fluent/fluentd-kubernetes-daemonset:v1.4-debian-forward-1 
        command: 
          - /bin/sh 
          - '-c'
          - >
            fluent-gem i fluent-plugin-grafana-loki-licence-fix ;
            tini /fluentd/entrypoint.sh;
        resources:
          limits:
            memory: 200Mi
          requests:
            cpu: 100m
            memory: 200Mi
        volumeMounts:
        - name: varlog
          mountPath: /var/log
        - name: varlibdockercontainers
          mountPath: /var/lib/docker/containers
          readOnly: true
        - name: config
          mountPath: /fluentd/etc
      terminationGracePeriodSeconds: 30
      volumes:
      - name: varlog
        hostPath:
          path: /var/log
      - name: varlibdockercontainers
        hostPath:
          path: /var/lib/docker/containers
      - name: config
        configMap:
          name: fluentd-logging
      serviceAccount: fluentd-service-account
      serviceAccountName: fluentd-service-account
```
<br>

##### Fluentd configmap 배포
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: fluentd-logging
  namespace: kube-system
  labels:
    app: fluentd-logging
data: 
  fluent.conf: |
    <source>
      @type tail
      path /var/log/*.log
      pos_file /var/log/fluentd/tmp/access.log.pos
      tag foo.*
      <parse>
        @type json
      </parse>
    </source>
    <source>
      @type tail
      @id in_tail_container_logs
      path /var/log/containers/*.log
      pos_file /var/log/fluentd-containers.log.pos
      tag kubernetes.*
      read_from_head true
      <parse>
        @type json
        time_format %Y-%m-%dT%H:%M:%S.%NZ
      </parse>
    </source>
    <match fluentd.**>
      @type null
    </match>
    <match kubernetes.var.log.containers.**fluentd**.log>
      @type null
    </match>
    <filter kubernetes.**>
      @type kubernetes_metadata
      @id filter_kube_metadata
    </filter>
    <filter kubernetes.var.log.containers.**>
      @type record_transformer
      enable_ruby
      remove_keys kubernetes, docker
      <record>
        app ${ record.dig("kubernetes", "labels", "app") }
        job ${ record.dig("kubernetes", "labels", "app") }
        namespace ${ record.dig("kubernetes", "namespace_name") }
        pod ${ record.dig("kubernetes", "pod_name") }
        container ${ record.dig("kubernetes", "container_name") }
      </record>
    </filter>
    
    <match kubernetes.var.log.containers.**>
      @type copy
      <store>
        @type loki
        url "http://loki.monitoring.svc.cluster.local:3100"
        # extra_labels {"env":"dev"}
        label_keys "app,job,namespace,pod"
        flush_interval 10s
        flush_at_shutdown true
        buffer_chunk_limit 1m
      </store>
      <store>
        @type stdout
      </store>
    </match>
```

<br>

##### RBAC & ServiveAccount 생성
```yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: fluentd-service-account
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: fluentd-service-account
subjects:
- kind: ServiceAccount
  name: fluentd-service-account
  namespace: kube-system

---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: fluentd-service-account
  namespace: kube-system
rules:
  - apiGroups: ["*"]
    resources:
      - pods
      - namespaces
    verbs:
      - get
      - watch
      - list

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: fluentd-service-account
  namespace: kube-system
```

