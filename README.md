<a href="https://yess-p.notion.site/Kubernetes-35ee8b3746684184abf9b0fe75d2b848"><img src="https://img.shields.io/badge/Kubernetes-326CE5?style=flat-square&logo=Kubernetes&logoColor=white&link=내링크"/></a>
<a href="https://yess-p.notion.site/Docker-3f8e94a739464249afa5237a045e743b"><img src="https://img.shields.io/badge/Docker-2496ED?style=flat-square&logo=Docker&logoColor=white"/></a>
<a href="https://yess-p.notion.site/AWS-8f04e2c4d4594af6bdebc452a2169d43"><img src="https://img.shields.io/badge/AWS-232F3E?style=flat-square&logo=Amazon AWS&logoColor=white"/></a>
<a href="https://yess-p.notion.site/Kubeadm-c12569bb986c450398af7c9653e51b9d"><img src="https://img.shields.io/badge/VMware-607078?style=flat-square&logoColor=white"/></a>
<a href="https://yess-p.notion.site/Terraform-8382fa798edc4d249fb9d1591d1ba37b"><img src="https://img.shields.io/badge/Terraform-7B42BC?style=flat-square&logo=Terraform&logoColor=white"/></a>
<a href="https://yess-p.notion.site/Jenkins-1c5e5b24d390416ca8a1038be9c783cf"><img src="https://img.shields.io/badge/Jenkins-D24939?style=flat-square&logo=Jenkins&logoColor=white"/></a>
<a href="https://yess-p.notion.site/Argo-CD-56909258c07048bfa09f8c42fc4e40c0"><img src="https://img.shields.io/badge/ArgoCD-61c3c5?style=flat-square&logoColor=white"/></a>
<a href="https://yess-p.notion.site/Prometheus-7ea57f31d5964eb68d011d1c3c47a499"><img src="https://img.shields.io/badge/Prometheus-E6522C?style=flat-square&logo=Prometheus&logoColor=white"/></a>
<a href="https://yess-p.notion.site/Monitoring-b8c9063ae6f340d1b9d2f4640e79cdef"><img src="https://img.shields.io/badge/Grafana-F46800?style=flat-square&logo=Grafana&logoColor=white"/></a>
<a href="https://yess-p.notion.site/Grafana-Loki-2723ec9207e74b159b3605100ac4b19b"><img src="https://img.shields.io/badge/Loki-f9bf52?style=flat-square&logoColor=white"/></a>
<a href="https://yess-p.notion.site/fluentd-fbb40d07ff1a424a8692f069907d1306"><img src="https://img.shields.io/badge/Fluentd-0E83C8?style=flat-square&logo=Fluentd&logoColor=white"/></a>
<a href="https://gitlab.com/Yess-P/corona-map/-/blob/main/corona-map-code/corona19.ipynb"><img src="https://img.shields.io/badge/Python-3766AB?style=flat-square&logo=Python&logoColor=white"/></a>
<a href="https://yess-p.notion.site/DevOps-7079eb5033b04c249631296de8b31da0"><img src="https://img.shields.io/badge/Notion-000000?style=flat-square&logo=Notion&logoColor=white"/></a>

# DevOps Infra
> Kubernetes, AWS, VMware, Terraform, Jenkins, ArgoCD, Prometheus, Grafana, Loki, Fluentd 를 활용하여 개발 환경을 구축하였습니다. <br>  
> 더 자세한 내용은 repository 와 https://yess-p.notion.site/DevOps-7079eb5033b04c249631296de8b31da0 를 참조해주세요.

<br>

Click for details

***
![DevOps](/uploads/fcee57105e52195fce8d8334f9da851a/DevOps.png)
***

