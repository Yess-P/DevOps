<a href="https://yess-p.notion.site/Terraform-8382fa798edc4d249fb9d1591d1ba37b"><img src="https://img.shields.io/badge/Terraform-7B42BC?style=flat-square&logo=Terraform&logoColor=white"/></a>
<a href="https://yess-p.notion.site/AWS-8f04e2c4d4594af6bdebc452a2169d43"><img src="https://img.shields.io/badge/AWS-232F3E?style=flat-square&logo=Amazon AWS&logoColor=white"/></a>

# AWS Infra
AWS 인프라를 구성을 위해 Terraform을 활용하여 아래와 같은 환경을 구축하였습니다.

![aws-drawio-Page-1 (1)](https://user-images.githubusercontent.com/79623220/125240138-a9adce80-e324-11eb-9684-927a6c9ae68a.jpg)
