# Kubeadm

<br>

> **Oracle VM VirtualBox**

---

**설치 전 설정**

VirtualBox에서 64비트 모드 사용을 위한 설정

1. 바이오스 모드 진입
2. Advanced Mode(F7) 진입
3. Advance 탭에서 CPU Configuration 클릭
4. SVM Mode를 Enabled(활성화)로 변경
5. PC를 켠 후 window일 경우 작업관리자에 성능탭에서 CPU 클릭 후 가상화가 사용으로 되어있는지 체크

<br>

**설치**

아래 링크로 사이트에 접속하고 각자 환경에 맞게 파일 다운로드 후 실행

[Downloads - Oracle VM VirtualBox](https://www.virtualbox.org/wiki/Downloads)



<br>

**네트워크 구성**

- 파일(F) > 환경설정 클릭 > 네트워크 탭 선택
- NatNetwork 추가(오른쪽 녹색 플러스) 후 수정
- 네트워크 이름 : k8s-Network, 네트워크 CIDR : 10.100.0.0/24
- 포트포워딩 클릭 후 아래와 같이 수정

   ![Untitled](/uploads/c5bfff086762da6b408892dbfc82d803/Untitled.png)

<br>

> **Ubuntu 20.04 LTS 다운로드**

---

사이트 접속 후 20.04 LTS Desktop iso 이미지 다운로드 

[Enterprise Open Source and Linux | Ubuntu](https://ubuntu.com/)

<br>

> **k8s의 master로 사용할 vm 생성 및 ubuntu:20.04 설치**

---

master를 하나 만들어서 환경구성 완료한 후 복제해서 node1, node2 만들 예정

<br>

**k8s master 가상머신 생성**

 <details>
 <summary>생성 과정</summary>
    
  <br>
  
  1. 새로만들기 클릭

  <br>

  2. 이름 및 운영체제 설정

     ![Untitled_1](/uploads/14be533b6add4b28ab00bf5d96296392/Untitled_1.png)

  3. 메모리 크기

     ![Untitled_2](/uploads/f45926a03b0e9ff445e4f415e199cce4/Untitled_2.png)

  4. 하드디스크

     ![Untitled_3](/uploads/8209056276d97d5e235bfce0f675199e/Untitled_3.png)


  5. 하드디스크 파일 종류

     ![Untitled_4](/uploads/973cb2bd7aaa8a43145f18c210af2e0f/Untitled_4.png)


  6. 파일 위치 및 크기

     ![Untitled_5](/uploads/aa43059a814f8707af890da4b89e2f19/Untitled_5.png)


  7. 만들기

     ![Untitled_6](/uploads/35ac9d90d4481a7c5e264424f08d9efb/Untitled_6.png)


  8. 시스템 클릭 후 시스템에서 플로피 제거

     ![Untitled_7](/uploads/7d0ed7a3a43369b9e4b042519173e31b/Untitled_7.png)


  9. CPU core 두 개로 변경

     ![Untitled_8](/uploads/2a4b3127a58bc17ec5f1b4d20293e52d/Untitled_8.png)


  10. 네트워크 클릭 후 k8s Network로 수정

      ![Untitled_9](/uploads/22cdb638e37bcafc16d81ebc921a84d7/Untitled_9.png)


  11. 부팅할 CD 구성
  
      ![Untitled_10](/uploads/2ff19b6ec55ba0378e3f0059bb13e2d1/Untitled_10.png)


  클릭 후 다운받은 Ubuntu 20.04 LTS 이미지 선택후 확인

  12. 실행 후 시작 클릭

      ![Untitled_11](/uploads/c1f89988981ed5194c82084a718defa0/Untitled_11.png)


 </details>


<br>

> **Ubuntu install 진행**

---

 일반적으로 Desktop에 설치하는 것과 같음

 <br>

**네트워크 구성**

 <details>
 <summary> 구성 과정 </summary>

  <br>

  설정 > 네트워크 > IPv4

  1. 자동(DHCP)로 되어있던 방식으로 수동으로 변경 

     **IP address** : 10.100.0.104/24 <br>
     **G/W** : 10.100.0.1  <br>
     **DNS** : 10.100.0.1  <br>

     ![Untitled_12](/uploads/f2afa8253943c8d099a7c716cbb50053/Untitled_12.png)


     네트워크 변경사항 적용을 위해 네트워크 탭에서 연결 버튼 클릭해 다운(down) 후 재가동

  <br>

  2. hostname &  hosts 변경

      ```bash
      sudo vi /etc/hostname

      # hostname 변경
      master.example.com

      sudo vi /etc/hosts

      # /etc/hosts에 내 호스트이름이랑 다른 호스트이름 등록
      # DNS가 없어서 이렇게 진행
      # 밑에 hosts 추가 후 저장 및 닫기
      10.100.0.004   master.example.com   master
      10.100.0.001   node1.example.com    node1
      10.100.0.002   node2.example.com    node2

      # ping test
      ping -c 2 8.8.8.8
      ping -c 2 www.google.com
      ```

 </details>

 <br>

 **설치된 ubuntu 20.04 LTS 정보 확인**

 ```bash
 # 커널 확인
 uname -r 
 
 # 메모리 확인
 free -h

 # cpu 확인
 lscpu

 # os 정보
 cat /etc/os-release
 ```

<br>

> **ssh server 설치 및 서비스 동작**

---

```bash
sudo apt-get update

sudo apt-get install -y openssh-server curl vim tree
```

<br>

> **클립보드 공유 & 드래그앤드롭 설정**

---

윈도우와 리눅스 가상머신 간에 copy & paste 를 위한 설정

<details>
 <summary> 설정 방법 </summary>

  <br>

  ![Untitled_13](/uploads/0a82f63aa9e7696cd43311b1cfbfe3bf/Untitled_13.png)

  ![Untitled_14](/uploads/e07fa2409db7ed3c8e98bb642f6f2e7f/Untitled_14.png)

  ![Untitled_15](/uploads/fe79438e7ef0d6d3f01e36b7c4fd6dfa/Untitled_15.png)

</details>

<br>

> **가상머신 부팅 방식을 GUI에서 text 로그인으로 변경**

---

```bash
systemctl set-default multi-user.target

# text모드로 변경
systemctl isolate multi-user.target

# graphic 모드로 변경
systemctl isolate graphical.target
```

<br>

> **xshell에서 접속**

---

**xshell 다운로드**

  아래 링크를 클릭하여 사이트 접속 후 다운로드

  [Xshell 다운로드](https://www.netsarang.com/ko/xshell-download/)

<br>

**설정**

<details>
 <summary> 설정 방법 </summary>

 <br>

 1. 새 새션 등록

      ![Untitled_16](/uploads/1272f8c1a6787985066910c253b08fa7/Untitled_16.png)


 2. 패스워드 설정

      ![Untitled_17](/uploads/51bc74a1e991959731b96b819d7a2c40/Untitled_17.png)

</details>

<br>

> **Docker install**

---

공식 홈페이지 그대로 다운로드 진행(Ubuntu version)

[Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)

<br>

> **가상머신 복제**

---

**복제 & 설정**

<details>
 <summary> 복제 과정 및 설정 과정 </summary>

 1. 복제 선택

     ![Untitled_18](/uploads/9207aec549741829ecdc6af7d67013d2/Untitled_18.png)

 2. 복제 설정

     ![Untitled_19](/uploads/a25afd64094ea13a8014049fd208d3ff/Untitled_19.png)


     ![Untitled_20](/uploads/d3a99c11fc1d7be49a377e5806365db9/Untitled_20.png)


     ![Untitled_21](/uploads/f91ab16e41293493b3b5837a9d4a07ed/Untitled_21.png)


     ![Untitled_22](/uploads/20a60f9fbb9873b64285115aac6d1754/Untitled_22.png)


 3. Ubuntu hostname & 네트워크 설정

     hostname 변경

     ![Untitled_23](/uploads/740f590ca20317ef238bd15a7bef0b24/Untitled_23.png)


     ![Untitled_24](/uploads/ac4531d16b546acc01484957ee688db3/Untitled_24.png)


     설정 후 네트워크 버튼을 down 후 다시 up 시켜 수정내용 적용

     ```bash
     # IP 설정 확인
     ip addr 

     # 통신 확인
     ping -c 2 master.example.com

     # hostname 확인
     hostname

     # cmd 모드로 설정
     systemctl isolate multi-user.target

     ```

</details>

<br>

> **Kubernetes**

---

쿠버네티스 공식 홈페이지

   [kubeadm 설치하기/ 공식홈페이지](https://kubernetes.io/ko/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)


**Kubernetes 설치**

<details>
 <summary> kubernetes 설치 과정 </summary>

  ```bash
  # Swap disabled
  swapoff -a && sed -i '/swap/s/^/#/' /etc/fstab

  # Letting iptables see bridged traffic(iptables 설정)
  cat <<EOF > /etc/sysctl.d/k8s.conf
    net.bridge.bridge-nf-call-ip6tables = 1
    net.bridge.bridge-nf-call-iptables = 1
  EOF
  sysctl --system

  # Disable firewall
  # error 날 것 입니다
  systemctl stop firewalld 
  systemctl disable firewalld

  # Set SELinux in permissive mode (effectively disabling it)
  setenforce 0
  sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

  # kubeadm, kubelet, kubectl 설치
  apt-get update && apt-get install -y apt-transport-https curl
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
  cat <<EOF | tee /etc/apt/sources.list.d/kubernetes.list
  deb https://apt.kubernetes.io/ kubernetes-xenial main
  EOF
  apt-get update

    # 최신 버전 설치
  apt-get install -y kubelet kubeadm kubectl
    # 버전을 선택하여 설치
  apt-get install -y kubelet=1.19.8-00 kubeadm=1.19.8-00 kubectl=1.19.8-00

  # 실행
  systemctl start kubelet && systemctl enable kubelet

  ```

  kubernetes version 정보는 아래 링크를 참조

  [kubernetes-version](https://github.com/kubernetes/kubernetes/tree/master/CHANGELOG)

  <br>

  **Master 컴포넌트 구성**

  ```bash
  # master 컴포넌트 생성
  kubeadm init

  # 일반 계정에서 사용가능하게 하는 코드(root 에서 실행)
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

  # 워커노드에서 마스터 노드에 연결할 때 필요한 토큰 저장
  cat > token.tx
  kubeadm join 10.100.0.104:6443 --token 1ou05o.kk...3 --discovery-token-ca-cert-hash sha256:8d9a7308ea6ff73.........576c112f326690
  <Ctrl>+<d>

  # CNI calico 설치
  curl https://docs.projectcalico.org/manifests/calico.yaml -O
  kubectl apply -f calico.yaml

  # bash shell에서 [TAB]키를 이용해 kubernetes command 자동완성 구성
  source <(kubectl completion bash)
  echo "source <(kubectl completion bash)" >> ~/.bashrc

  source <(kubeadm completion bash)
  echo "source <(kubeadm completion bash)" >> ~/.bashrc
  ```

  ![img1.daumcdn](/uploads/5b38f804c0c744a6de4ffc2a55d6ed91/img1.daumcdn.png)

  **Worker 노드 구성**

  ```bash
  # master 노드와 연동
  kubeadm join 10.100.0.104:6443 --token 1ou05o.kk...3 --discovery-token-ca-cert-hash sha256:8d9a7308ea6ff73.........576c112f326690
  ```

</details>

<br>

> **호스트 전용 어댑터**

---

**외부 접속을 위한 호스트 전용 어댑터 설정**

<details>
 <summary> 구성 </summary>

  <br>

  PC 종료 후 아래와 같이 설정

  ![Untitled_25](/uploads/4351ffa1aaf1d0e9b9d2c9a34fbb18f8/Untitled_25.png)

</details>

<br>

> **Ingress Controller**

---

<details>
 <summary> Ingress Controller 설치 & 구성 </summary>

  ```bash
  kubectl get nodes

  # git 설치
  apt install git -y

  # git에서 nginx 파일 다운로드
  git clone https://github.com/nginxinc/kubernetes-ingress/

  # 버전 확인
  cd kubernetes-ingress/deployments
  git checkout v1.9.0

  # nginx-ingress 실행
  kubectl apply -f common/ns-and-sa.yaml
  kubectl apply -f rbac/rbac.yaml
  kubectl apply -f common/default-server-secret.yaml
  kubectl apply -f common/nginx-config.yaml
  kubectl apply -f common/ingress-class.yaml
  kubectl apply -f daemon-set/nginx-ingress.yaml

  # namespace 확인
  kubectl get ns

  # nginx-ingress로 생성된 pod 확인
  kubectl get pod --namespace nginx-ingress

  # pod 자세히 확인
  kubectl describe pod nginx-ingress-g9ffq --namespace nginx-ingress

  # pod log 확인
  kubectl logs nginx-ingress-g9ffq --namespace nginx-ingress
  ```

</details>

<br>

> **MetalLb**

---
<details>
 <summary> MetalLB 설치 & 구성 </summary>

  <br>

  ```bash
  # namespace 확인
  kubectl get ns

  # MetalLB 설치 및 적용
  kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.9.3/manifests/namespace.yaml
  kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.9.3/manifests/metallb.yaml
  kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
  kubectl get ns

  # MetalLB 구성 시스템 확인
  kubectl get pods,serviceaccounts,deployments,roles,rolebindings -n metallb-system

  # config 파일 실행(구성은 밑에 code 참조)
  kubectl apply -f config.yaml

  # hotellb pod 실행(hotellb 코드는 밑에 code 참조)
  kubectl apply -f hotellb.yml
  kubectl get ns

  # hotel namespace로 적용된 pod, ingress, service 확인
  kubectl get pods -o wide --namespace hotel
  kubectl get ingress --namespace hotel
  kubectl get svc --namespace hotel
  ```

  **config 파일 구성**

  ```yaml
  apiVersion: v1
  kind: ConfigMap
  metadata:
    namespace: metallb-system
    name: config
  data:
    config: |
      address-pools:
      - name: default
        protocol: layer2
        addresses:
        - 10.10.10.205-10.10.10.210   # 실행될 pod들에 적용될 IP

  ```

  **hotellb.yaml**

  ```yaml
  apiVersion: v1
  kind: Namespace
  metadata:
    name: hotel

  ---

  apiVersion: extensions/v1beta1
  kind: Ingress
  metadata:
    name: hotel-ingress
    namespace: hotel
  spec:
    ingressClassName: nginx
    rules:
    - host: hotel.example.com/ 
      http:
        paths:
        - path: /
          backend:
            serviceName: hotel-svc
            servicePort: 80
          
  ---

  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: hotel
    namespace: hotel
  spec:
    replicas: 2
    selector:
      matchLabels:
        app: hotel
    template:
      metadata:
        labels:
          app: hotel
      spec:
        containers:
        - name: hotel
          image: nginxdemos/hello:plain-text
          ports:
          - containerPort: 80
  ---

  apiVersion: v1
  kind: Service
  metadata:
    name: hotel-svc
    namespace: hotel
  spec:
    ports:
    - port: 80
      targetPort: 80
      protocol: TCP
      name: http
    selector:
      app: hotel
    type: LoadBalancer
  ```
</details>

<br>

> **HA 구성**

---

![kubeadm-ha-topology-stacked-etcd.svg](/uploads/829a50714fd277e9913149444002f6d7/kubeadm-ha-topology-stacked-etcd.svg)

**구성**

- 1 Load Balance server
- 3 Master server
- 2 Worker Node server


![Untitled_26](/uploads/cd4293869b5a4208372c6990c9f35c48/Untitled_26.png)

<br>

<details>
 <summary> 구성 방법 </summary>

 <br>

  1. **컨테이너 runtime 설치**

      Kubernetes는 기본으로 CRI(Container Runtime Interface)를 사용하여 컨테이너 런타임과 연동됩니다.

      - Docker - /var/run/docker.sock
      - containerd - /run/containerd/containerd.sock
      - CRI-O - /var/run/crio/crio.sock

      <br>

      docker 설치는 위에 있는 docker 설치와 동일합니다.

  <br>

  2. **Kubernetes 설치**

      ```bash
      **# Debian**

      # apt 업데이트 & 패키지 다운로드
      sudo apt-get update
      sudo apt-get install -y apt-transport-https ca-certificates curl

      # 구글 클라우드의 공개 signing key 다운로드
      sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

      # kubernetes apt repository 추가
      echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

      # install
      sudo apt-get update
      sudo apt-get install -y kubelet kubeadm kubectl
      sudo apt-mark hold kubelet kubeadm kubectl
      ```

      ```bash
      **# Red-Hat**

      cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
      [kubernetes]
      name=Kubernetes
      baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
      enabled=1
      gpgcheck=1
      repo_gpgcheck=1
      gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
      exclude=kubelet kubeadm kubectl
      EOF

      # Set SELinux in permissive mode (effectively disabling it)
      sudo setenforce 0
      sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

      sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

      sudo systemctl enable --now kubelet
      ```

  <br>

  3. **Multi-Master의 단일 진입점인 LoadBalancer 구성, 사실상 HA 클러스터 구성**

      별도의 LoadBalance 장비가 없으므로 10.100.0.254 IP를 사용하는 Nginx를 이용하여 LoadBalance를 구성합니다

      ```bash
      **# Nginx가 LoadBalancer 역할을 할 수 있도록 설정**

      mkdir /etc/nginx
      cat << END > /etc/nginx/nginx.conf
        > events { }
        > stream {
              upstream stream_backend {
                  least_conn;
                  server 10.100.0.104:6443;
                  server 10.100.0.105:6443;
                  server 10.100.0.106:6443;
              }
            
              server {
                  listen        6443;
                  proxy_pass    stream_backend;
                  proxy_timeout 300s;
                  proxy_connect_timeout ls;
              }
      }
      END

      # 6443 port를 listen 하고 있다가 요청이 오면 stream_backend로 전달하겠다
      ```

      Nginx command

      - Proxy_pass - 요청이 백엔드 서버로 전달되게 지정하는 command 입니다.

      <br>

      그외 command는 아래 블로그 참조
      
      [[Nginx] 엔진엑스 프록시 모듈](https://12bme.tistory.com/367)

      ```docker
      **# 도커 컨테이너로 Nginx를 실행하여 LB를 운영**

      docker run --name proxy -v /etc/nginx/nginx.conf:/etc/nginx/nginx.conf:ro --restart=always -p 6443:6443 -d nginx

      # --name           : 이름 설정
      # -v               : volume mount를 통해 위에 설정한 설정 값 적용
      # --restart=always : 항상 docker deamon과 같이 실행
      # -p               : 시스템의 6443 port가 docker 6443 port로 portforward
      # -d               : background 실행
      ```

  <br>

  4. **Kubeadm을 이용한 HA 클러스터 구성**

      1) master1: kubeadm init 명령으로 초기화 - LB 등록

      ```bash
      # Kubeadm 설치 & LB 등록
      Kubeadm init --control-plane-endpoint "lb.example.com:6443" --upload-certs

      # Kubectl을 사용하기 위한 인증서 등록
      mkdir -p $HOME/.kube
      sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
      sudo chown $(id -u):$(id -g) $HOME/.kube/config
      ```

      2) master2, master3을 master1에 join

      ```bash
      # kubeadm으로 join
      kubeadm join lb.example.com:6443 --token ~~~~~~~\
        --discovery-token-ca-cert-hash ~~~~~~~\
        --control-plane --certificate-key ~~~~~~~

      # Kubectl을 사용하기 위한 인증서 등록
      mkdir -p $HOME/.kube
      sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
      sudo chown $(id -u):$(id -g) $HOME/.kube/config
      ```

      ---

      ![kubeadm-get-node](/uploads/95eefd1ee0e294bba806ca6b95dbab79/kubeadm-get-node.png)


      CNI를 설치하지 않아서 STATUS가 NotReady 상태이다

      ---

      3) CNI(Container Network Interface.weave) 설치 : master1

      ```bash
      # CNI인 weave.works 설치
      kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

      # Node 확인
      kubectl get nodes
      ```

      ---

      ![kubeadm-get-node1](/uploads/739553f3efea13c68d20afe63a63d0ca/kubeadm-get-node1.png)

      ---

      4) worker node를 LB를 통해 master와 join

      ```bash
      # kubeadm으로 join
      kubeadm join lb.example.com:6443 --token ~~~~~~~\
        --discovery-token-ca-cert-hash ~~~~~~~\

      # Kubectl을 사용하기 위한 인증서 등록
      mkdir -p $HOME/.kube
      sudo scp master1:/etc/kubernetes/admin.conf $HOME/.kube/config
      sudo chown $(id -u):$(id -g) $HOME/.kube/config

      # Node 확인
      kubectl get nodes
      ```

      ---

      ![kubeadm-get-node2](/uploads/d2b8b24920f37b8006c7e10cddd12550/kubeadm-get-node2.png)

      ---
