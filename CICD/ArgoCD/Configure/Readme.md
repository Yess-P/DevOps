# Argo CD  구성

<br>

> **Repository 등록**

---

Menu Bar에서 2번째에 위치한 톱니바퀴 모양의 아이콘(Manage your repositories, projects, settings)을 클릭 → Repositories 클릭 → CONNECT REPO USING SSH 클릭

<br>

---

![ArgoCD1](/uploads/5034d5ba9d98a3ad67d9b5b2291d861a/ArgoCD1.png)

![first](/uploads/982d1972a46394ca0fe306f649a1a292/first.png)

---

<br>
<br>

---

 ![ArgoCD2](/uploads/52568c92f3ade9fcaae703140de32aa8/ArgoCD2.png)

---

설정을 하시고 CONNECT를 누르시면 error가 없을 시 위와 같이 `CONNECTION STATUS`가 `Successful`로 나오게 됩니다. 그리고 Application 생성을 위해 점 3개가 있는 곳을 클릭하시고 `Create application` 을 선택해주세요.

<br>

> **Create Application**
 ---

 <br>

 ##### First Section

 ---

 ![ArgoCD3](/uploads/1eefebe047cc49c2f3edef1c66e2d840/ArgoCD3.png)

 ![second](/uploads/aba05bb54bdb7da5fe0468b92008f8e1/second.png)

 ---

 <br>
 <br>

 ##### Second Section

 --- 

 ![ArgoCD4](/uploads/4a4637cfaab076478b0d221e3c98cf9a/ArgoCD4.png)


 ![Third](/uploads/7844d8769e978fd39e680fae524643a3/Third.png)

 ---

 <br>
 <br>

 ##### Third Section

 ---

 ![ArgoCD5](/uploads/b797a193d53b1abf897d507ed6c8bd87/ArgoCD5.png)

 ![fourth](/uploads/f79223c5593461ab46b0abc0139c3481/fourth.png)

 ---

 <br>
 <br>

 
 ##### Fourth Section
 ---

 ![ArgoCD6](/uploads/3452746362014529e91466fa3dec4c89/ArgoCD6.png)

 ---

 만약 Helm 차트가 git에 있다면 자동으로 values 파일의 값들이 입력됩니다. 각자 상황에 맞게 입력해주시면 됩니다. 다만 처음만든 형태와 차트가 다르다면 error가 발생합니다.

 이제 crate을 누르면 배포가 완료됩니다.
