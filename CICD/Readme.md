<a href="https://yess-p.notion.site/Jenkins-1c5e5b24d390416ca8a1038be9c783cf"><img src="https://img.shields.io/badge/Jenkins-D24939?style=flat-square&logo=Jenkins&logoColor=white"/></a>
<a href="https://yess-p.notion.site/Argo-CD-56909258c07048bfa09f8c42fc4e40c0"><img src="https://img.shields.io/badge/ArgoCD-61c3c5?style=flat-square&logoColor=white"/></a>

# CICD Pipeline

<br>

###### CICD 구성도 및 전체 FlowChart 입니다

***
![flowchart](/uploads/223b912372bfdd29e5ee8baa07a9557b/flowchart.png)
***
