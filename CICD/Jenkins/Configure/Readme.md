### Plugin 설치
***

 Docker, Gitlab 사용 및 다양한 구성을 위해 Plugin을 설치해야 합니다  

 설치 위치 : Jenkins 관리 → 플러그인 관리(Plugin manage) → 설치가능 → 검색

 <details>
 <summary>Plugin List</summary>

 - `Docker Pipeline` <br>
 - `Docker plugin ` <br>
 - `GitHub Integration Plugin` <br>
 - `Gitlab API Plugin` <br>
 - `Gitlab Authentication Plugin` <br>
 - `Gitlab Plugin` <br>
 - `Pipeline` <br>
 - `Pipeline:Github` <br>
 - `Pipeline Utility Steps` <br>
 - `SSH Agent Plugin` <br>
 - `Workspace Cleanup Plugin` <br>

 </details>  

 <br>

### Credential
***

 Docker, Git, AWS 플랫폼과의 연동을 위해 Credential을 등록해줘야 합니다

 <details>
 <summary> AWS Credential </summary>

 <br>

 access key ID & access key password를 따로따로 등록해줍니다  

 - Kind - Secret text
    - Scope -Global
    - Secret - access key ID or access key password
    - ID - pipeline에서 사용할 변수 이름
    - Description - 설명
    
    <br>

    ---

    ![Untitled](/uploads/e5882602688ec0942b55294248c6a37e/Untitled.png)

    ---

    <br>

 > Jenkins & AWS 연동을 위한 Pipeline 설정
 > ```JavaScript
 > pipeline {
 >	enviroment {
 >	  AWS_ACCESS_KEY_ID = credentials('awsAccessKeyId')
 >	  AWS_SECRET_ACCESS_KEY = credentials('awsSecretAccessKey')
 >	  //Region은 등록할 필요 없음
 >		AWS_DEFAULT_REGION = 'ap-northeast-2'
 >		}
 > }
 > ```

 </details>


<details>
<summary> Gitlab Credential </summary>

<br>

HTTP는 git push를 할 수 없어 API Token 과 함께 사용 해야 하지만 사용시 password나
Token이 노출되는 단점이 있어 필히 2FA와 함께 사용해야 합니다 

SSH는 git push & 비밀번호 노출에 대해 자유롭지만 초기에 더 많은 설정을 해야합니다

Git push 사용이 없이 clone만 사용한다면 HTTP가 간단할 것이고, Git push를 사용한다면
설정을 조금 더 하더라도 SSH를 사용하는 것을 추천합니다

<br>

<details>
 <summary> HTTP </summary>

 <br>

 - Kind - Username with password
     - Scope -Global
     - Username - Git 아이디
     - Password - Git 패스워드
     - ID - pipeline에서 사용할 변수 이름
     - Description - 설명

 <br>

 ---

 ![Untitled_1](/uploads/55c4a93d101d30c6bf350e7dd58389f5/Untitled_1.png)

 ---

 <br>
    
</details>

<details>
<summary> SSH </summary>

<br>

 <details>
 <summary> SSH 사용을 위한 등록 </summary>

##### Jenkins 폴더에서 SSH key 생성

`1. Jenkins가 위치한 폴더로 이동 & 권한 설정`
 ```bash
 su -
 cd /var/lib/jenkins
 mkdir .ssh
 chown jenkins:jenkins .ssh
 chmod 700 .ssh
 cd .ssh
 ```

`2. public key & private key 생성` <br>
 ```bash
 ssh-keygen -t rsa -b 4096 -C "parkes9509@gmail.com"
 ```

 아래와 같이 나온다면 명령어가 나온다면
 ```bash
 Enter file in which to save the key (/root/.ssh/id_rsa):
 ```
 위치 & 이름 입력합니다  
 마지막 yess는 private key & public key의 이름입니다
 ```bash
 /var/lib/jenkins/.ssh/yess 
 ```

 key의 소유권 변경 & 사용자 전용으로 권한 변경
 ```bash
 chown jenkins:jenkins yess
 chown jenkins:jenkins yess.pub
 chmod 600 yess
 chmod 600 yess.pub
 ```

 .ssh/config 생성
 User는 크게 상관없습니다
 왜냐하면 실제로 ssh로 접속할 때는 key만 비교해서 접속하기 때문입니다
 ```bash
 vi config
 ```
	
 > config
 > ```bash
 > Host gitlab.com 
 > HostName gitlab.com 
 > User Yess-p 
 > IdentityFile ~/.ssh/yess
 > ```

 ```bash
 chown jenkins:jenkins config
 chmod 600 config
 ```

 <br>

##### Gitlab or Github에 SSH 공개 key 등록

 - 전체 repository 사용 <br>
   Github or Gitlab 접속 후 로그인 → User Settings → SSH Keys

   ---

   ![Untitled_2](/uploads/e1bb484978664993a4033cc2ad23fb35/Untitled_2.png)

   ---

   <br>
   <br>

 - 특정 repository 사용 <br>
   Github or Gitlab 접속 후 로그인 → Project → repo 선택 → 왼쪽 bar에 위치한 setting → Repository 클릭 → Deploy keys에서 Expand 클릭 → Key에 공개키 입력 후 Grant write permissions  to this key 클릭 → Add key 클릭

   ---

   ![Untitled_3](/uploads/7f0fd80ef55ee35e518ad5d05895757d/Untitled_3.png)

   ---

   <br>

   

</details>

<details>
<summary> Credential 등록 </summary>

<br>

- Kind - Username with password
    - Scope -Global
    - Username - Git 아이디
    - Password - Git 패스워드
    - ID - pipeline에서 사용할 변수 이름
    - Description - 설명

    <br>

    ---

    ![Untitled_4](/uploads/e5109490ace53243b41f82de68f515f2/Untitled_4.png)

    ---

    <br>

</details>

</details>
<br>
</details>

<details>
<summary> Docker Credential </summary>

<br>

- Kind - Username woth password
    - Scope -Global
    - Username - Docker 아이디
    - Password - Docker 패스워드
    - ID - pipeline에서 사용할 변수 이름
    - Description - 설명

    <br>

    ---
    
    ![Untitled_5](/uploads/2fb33bdf45e92e712d8aff72762e7954/Untitled_5.png)

    ---

    <br>

> Jenkins & Docker 연동을 위한 Pipeline 설정
> ```Groovy
> pipeline {
>	  agent any
>   stages {
>     stage('Bulid Backend') {
>        agent any
>          steps {
>            echo 'Build Image'
>              script{
>                docker.withRegistry( 'https://registry.hub.docker.com', 'docker_credential' ) {
>                  image = docker.build("parkes9509/corona:0.0.5")
>                    }
>              }
>          }
>        }
>	  
>    }
> }
> ```

</details>

