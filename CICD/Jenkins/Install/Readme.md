# **Jenkins Install**
Jenkins를 <b>`Local Server`</b> 에 설치 & Helm을 통해 <b>`Kubernetes Resource`</b>로 배포, 이 두 방식을 사용하여 설치하였습니다

<br>

> <b> Local Install <b>

---

###### Jnekins 패키지 추가
```bash
sudo yum update -y
sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins.io/redhat/jenkins.repo && 
sudo rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key
```

###### install java, docker, git
```bash
sudo yum install -y java-1.8.0-openjdk jenkins git docker
```


###### 자바 버전 8로 설정
```bash
sudo alternatives --config java
```

###### Docker사용 허용
```bash
sudo usermod -aG docker jenkins
sudo usermod -aG docker ec2-user
```

###### Jenkins 시작
```bash
sudo service jenkins start
```

###### Docker 시작 및 권한 수정
```bash
sudo systemctl start docker
sudo chmod 666 /var/run/docker.sock
```

###### 종료
```bash
sudo service jenkins stop
```

###### 웹접속, 인스턴스 ip + 8080 포트
```bash
13.209.70.150:8080

sudo cat /var/lib/jenkins/secrets/initialAdminPassword
011627a8eaa14914bdf4e13128397b8e
```

<br>

> <b> Helm Install <b>

---

###### Repo 추가 및 파일 fetch
```bash
helm repo add jenkins https://charts.jenkins.io

helm fetch --untar jenkins/jenkins

helm install jenkins ./jenkins

# ID & password
	ID : admin
	PW : printf $(kubectl get secret jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode);echo
```

> jenkins repo 안에 있는 values.yaml 의 수정 위치
> ```yaml
> # line 110	
> servicePort: 80
> targetPort: 8080
> # For minikube, set this to NodePort, elsewhere use LoadBalancer
> # Use ClusterIP if your setup includes ingress controller
> serviceType: LoadBalancer
> # Jenkins controller service annotations
> # alb로 설정해도 elb로 바뀐다
> serviceAnnotations:
>   service.beta.kubernetes.io/aws-load-balancer-type: "elb"
> ```
