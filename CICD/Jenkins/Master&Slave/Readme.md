# Master & Slave

Jenkins는 git & gitlab 의 webhook을 통해 push 이벤트가 발생했을 때 자동으로 job이 실행됩니다. 이로인해 서비스가 커지면 주기적으로 빌드와 배포가 되기 때문에 그 만큼 시스템에 부하가 생길 수 있는데, 이 부하를 분산시키기 위해 Master & Slave로 이루어진 Jenkins Cluster를 구성하는 것 입니다.

Master & Slave를 구성하는 방법은 몇 가지가 있지만 저는 ssh를 사용하여 연동하는 방법을 사용하겠습니다.

<br>

> **구성을 위한 플러그인**

---

구성을 위해서는 `SSH Build Agents plugin`이 설치되어 있어야 합니다. 기본적으로 설치되는 플러그인 이긴 하나 확인해보시기 바랍니다.

<br>

## Master

<br>

> **ssh-key 생성 & 등록**

---

먼저 `Master`에서는 `Slave` 접속을 위한 ssh-key가 필요합니다. 만약 git 혹은 gitlab 연동을 위해 ssh-key를 생성하셨다면 따로 생성하실 필요는 없습니다.

<br>

---

![jenkins-slave-ssh](/uploads/2d5cac59b9defcbb4f7302afa12ecbaf/jenkins-slave-ssh.png)

---

위와 같이 credential에 ssh pirvate key를 등록해 주시면 됩니다. 만약 git 적용을 위해서 등록하신다면 `Username` 부분만 `Master`에서 `Slave`에 접근하고자 하는 계정을 입력해 주시면 slave에 접속할 때도 사용가능 합니다(저는 ec2-user로 사용했습니다).

<br>

## Slave

<br>

> **설치**

---

`Master`에서 `Slave`에 접속하고 job을 수행하기 위해서 설치되는 프로그램입니다. 만약 java-1.8.0-openjdk 설치하지 않으실 시 접속하실 때 error가 발생합니다.

```bash
sudo yum install -y java-1.8.0-openjdk git docker
```

<br>

> **SSH-Public-Key 수신을 위한 설정**

---

`Master`에서 생성된 Public-Key를 수신하기 위해서는 몇가지 설정이 필요합니다.

```bash
# 암호 설정
sudo passwd
sudo passwd ec2-user

# 비밀번호로 접속을 위한 설정
sudo vi /etc/ssh/sshd_config
	...
	PasswordAuthentication yes
	...

# 공개키 등록
# /home/ec2-user
# 이미 파일일 존재한다면 chmod만 수행하세요
mkdir /home/ec2-user/.ssh
chmod 700 /home/ec2-user/.ssh
touch /home/ec2-user/authorized_keys
chmod 644 /home/ec2-user/.ssh/authorized_keys

# **Master**에서 Private-Key 전송
# 각자 위치를 수정해서 전송하세요
# 반드시 **Master**에서 실행해야됩니다! 
sudo scp /var/lib/jenkins/.ssh/id_rsa.pub [ec2-user@192.168.10.35](mailto:ec2-user@192.168.10.35):/home/ec2-user/.ssh/id_rsa.pub

# 공개키를 authorized_keys 에 등록
cat /home/ec2-user/.ssh/id_rsa.pub >> /home/ec2-user/.ssh/authorized_keys
```

<br>

## Web 설정

<br>

> **신규 노드 추가**

---

⚙️Jenkins 관리 → 🖥️노드관리 → 신규 노드를 클릭합니다. 노드명을 입력하시고 Permanent Agent를 클릭하신 후 OK 누릅니다.

<br>

---

![jenkins-slave](/uploads/6cdff54666063112b8b566a239745fc3/jenkins-slave.png)

![jenkins-slave2](/uploads/a460fed77898a90e31eeae29920c8525/jenkins-slave2.png)

---

![노드설정](/uploads/c97902232cfd32056096b7641e9af70e/노드설정.png)

<br>

위와 같이 설정을 하고 `Save` 를 클릭합니다.

<br>

---

![jenkins-slave3](/uploads/e98624212c1b4a4af144b59e1c5abf2f/jenkins-slave3.png)

---

<br>

> **Slave node 사용 방법**
---

<br>

```bash
# label을 지정하여 사용 가능하다
pipeline {
	agent{
		label 'slave'
		}

# 혹은 아래와 같이 사용 가능하다
pipeline {
	agent{
		label 'Master'
		}

   stages {
		stage('Deploy Clinet'){
			...
				}
		stage('Deploy Server'){
			agent{
				label 'slave'
				}
			steps{
				}
			}
		}
}
```
