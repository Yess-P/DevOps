# Pipeline
 Jenkins Pipeline을 <b>`Git Clone`</b> <b>`Docker Bulid`</b> <b>`Docker run`</b> <b>`Image push`</b> <b>`GitOps Checkout`</b>, 이렇게 5 stage로 나누어 구성하였습니다. <br>
 개발자가 코드를 Git에 업데이트시 Jenkins는 webhook을 통해 Pipeline을 실행 시킵니다.

 <br>

 > <b>Git Clone</b>
   ---
   Application 코드가 저장된 Git을 Clone 합니다.

   ```javascript
   stage('Git Clone') {
            agent any
            
            steps {
                echo 'Clonning Repository'
							
                git url: 'git@gitlab.com:Yess-P/corona-code.git',
                    branch: 'master',
                    credentialsId: 'gitlab-ssh'
                    
                sh ('git remote set-url origin git@gitlab.com:Yess-P/corona-code.git')
                }
                
        }
   ```

 <br>

 > <b>Docker Bulid</b>
   ---
   'RELEASE_NOTES' 라는 변수 생성과 <b>`Docker Bulid`</b> 를 진행합니다.
   - 'RELEASE_NOTES' 에는 Docker Image의 버전이 들어갑니다. 버전을 입력하기 위해 Git의 'commit message' 를 매개체로 사용하였습니다. <b>`Git Clone`</b>이 실행된 repository의 마지막 commit message가 버전이 되는 것입니다. 이를 위해 개발자는 git commit시 commit message를 버전과 맞춰야합니다.
   이렇게 하는 이유는 개발자가 Git에 코드 업데이트시 Jenkins를 통해 진행되는 CI 과정을 자동화하고 
   Jenkins를 통해 만들어지는 Docker Image를 식별할 뿐 아니라, Docker Build & Docker Image Push 진행시 버전을 통일하기 위함입니다.

   - 'RELEASE_NOTES'를 사용하여 <b>`Docker Bulid`</b>를 진행합니다.

   ```javascript
   stage('Docker Bulid') {
            agent any
            steps {
              echo 'Build Image'
              script{
                RELEASE_NOTES = sh (script: """git log --pretty=format:%s -1 """, returnStdout:true)
                echo ("${RELEASE_NOTES}")
                
                docker.withRegistry( 'https://registry.hub.docker.com', 'docker-cred' ) {
                  image = docker.build("parkes9509/corona:${RELEASE_NOTES}")
                    }
                }
              }
          } 

   ```

   <br>

 > <b>Docker run</b>
   ---
   <b>`Docker run`</b>에서는 <b>`Docker Bulid`</b>통해 생성된 이미지를 실행시킵니다.

   ```javascript
   stage('Docker run') {
          agent any

          steps {
            echo 'Docker run'

            script {
                image.run()
                }
          }
        }

   ```

  <br>

 > <b>Image push</b>
   ---

   <b>`Image push`</b>에서는 Docker credential를 사용하여 이미지를 Dockerhub에 등록합니다.

   ```javascript
   stage('Image push') {
          steps {
            echo 'Image push to Docker hub'

            script {
              docker.withRegistry( 'https://registry.hub.docker.com', 'docker-cred' ) {
                    image.push("${RELEASE_NOTES}")
                      }
              }
            }    
          }

   ```

   <br>

 > <b>GitOps Checkout</b>
   ---
   <b>`GitOps Checkout`</b>에서는 Helm으로 구성된 yaml 파일을 Clone하여 values.yaml에 위치한 이미지 버전을 'RELEASE_NOTES'로 수정 후 Git을 업데이트합니다.
   ```javascript
   stage('GitOps Checkout') {
          agent any
            steps {
                git url: 'git@gitlab.com:Yess-P/argo-yaml.git',
                    branch: "master",
                    credentialsId: 'gitlab-ssh'
                
                dir("mychart/"){
                    script{ datas = readYaml file: 'values.yaml' 
                        datas.version = RELEASE_NOTES
                    }
                
                    echo datas.version
                    
                    sh "rm values.yaml"
                    
                    script{ writeYaml file: 'values.yaml', data: datas}
                    
                    sh"cat values.yaml"
                }
                

                withCredentials([sshUserPrivateKey(credentialsId: 'gitlab-ssh', keyFileVariable: 'id_rsa_gitlab_argocd')]){
                    sh('git remote set-url origin git@gitlab.com:Yess-P/argo-yaml.git')
    
                    sh("git add .")
                    sh("git commit -m '${RELEASE_NOTES}'")
                    sh("git push -u origin master")
                     }             



            }
        }

   ```



